# Nordicsemi nRF5 Toolchain Docker Image

## Reference 
1. [Installing the toolchain](https://infocenter.nordicsemi.com/index.jsp?topic=%2Fcom.nordic.infocenter.meshsdk.v3.2.0%2Fmd_doc_getting_started_how_to_toolchain.html&anchor=toolchain_build_environment_ses)


### Building 

```
docker build --rm -f "Dockerfile" -t netzon-docker-nordicsemi-nrf5-toolchain:latest .
```

### Running

```
docker run -it netzon-docker-nordicsemi-nrf5-toolchain:latest /bin/bash
```