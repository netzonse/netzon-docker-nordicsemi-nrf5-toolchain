FROM phusion/baseimage:master

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

WORKDIR /opt/work/deps

RUN apt-get update 
RUN apt-get install -y cmake cmake-curses-gui wget virtualenv

RUN wget "https://netzon.blob.core.windows.net/static/docker/nRF-Command-Line-Tools_10_4_1_Linux-amd64.tar.gz"
RUN tar xf "nRF-Command-Line-Tools_10_4_1_Linux-amd64.tar.gz"
RUN dpkg -i "JLink_Linux_V650b_x86_64.deb" &&  apt-get install -y -f
RUN dpkg -i "nRF-Command-Line-Tools_10_4_1_Linux-amd64.deb" &&  apt-get install -y -f

RUN wget "https://netzon.blob.core.windows.net/static/docker/JLink_Linux_V654a_x86_64.deb"
RUN dpkg -i "JLink_Linux_V654a_x86_64.deb" &&  apt-get install -y -f

COPY deps/bash/.bashrc /root/.bashrc

RUN virtualenv -p python3 /opt/work/.env

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /opt/work/deps
WORKDIR /opt/work
